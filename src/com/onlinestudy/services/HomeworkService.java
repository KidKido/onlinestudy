package com.onlinestudy.services;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onlinestudy.dao.HomeworkDao;
import com.onlinestudy.entities.Homework;

@Service
public class HomeworkService {

	@Autowired
	private HomeworkDao homeworkDao;

	@Transactional
	public List<Homework> getHomeworksList(Integer limit, Integer offset, Integer teacherId) {
		List<Homework> homeworksList = homeworkDao.getHomeworksList(limit, offset, teacherId);
		
		for (Homework each : homeworksList) {
			each.setFormattedCreationDate(new SimpleDateFormat("dd MMMM yyyy о HH:mm", new Locale("uk", "UA")).format(each.getCreationDate()));
		}
		
		return homeworksList;
	}
	
	@Transactional
	public Boolean save(Homework homework) {
		return homeworkDao.save(homework);
	}

	@Transactional
	public List<Homework> getHomeworksList(Integer lessonId) {
		List<Homework> homeworksList = homeworkDao.getHomeworksList(lessonId);
		for (Homework each : homeworksList) {
			each.setFormattedCreationDate(new SimpleDateFormat("dd MMMM yyyy о HH:mm", new Locale("uk", "UA"))
					.format(each.getCreationDate()));
		}
		return homeworksList;
	}

	@Transactional
	public Homework getHomework(Integer homeworkId) {
		Homework homework = homeworkDao.getHomework(homeworkId);

		homework.setFormattedCreationDate(new SimpleDateFormat("dd MMMM yyyy о HH:mm", new Locale("uk", "UA")).format(homework.getCreationDate()));

		return homework;
	}
	
}
