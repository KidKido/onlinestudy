package com.onlinestudy.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlinestudy.services.LocationService;

@Controller
public class LocationController extends BaseController {
	
	@Autowired(required = false)
	private LocationService locationService;
	
	@RequestMapping(value = "/locationGetCitiesList.action", method = RequestMethod.POST)
	@ResponseBody
	public String postGetCitiesList(@RequestParam(value = "regionId", required = true) Integer regionId) throws JsonProcessingException {
		
		ObjectMapper mapper = new ObjectMapper();
		
		return mapper.writeValueAsString(locationService.getCitiesList(regionId));
	}

}
