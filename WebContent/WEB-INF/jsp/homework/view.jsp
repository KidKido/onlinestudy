<%@ include file="../custom/head.jsp"%>
<%@ include file="../custom/header.jsp"%>

<div class="container">
	
	<div class="content">
		
		<div class="content__header">
			<h1 class="title">
				${homework.getLesson().getTitle()}: ${pageTitle} <span class="content__block-title-description">${homework.getLesson().getSubject().getName()}</span>
			</h1>
		</div>
		
		<div class="content__block-header">
			<div class="content__block-header-item content__block-header-item--date">${homework.getFormattedCreationDate()}</div>
			<div class="content__block-header-item content__block-header-item--user">${homework.getTeacher().getName()}</div>
		</div>
		
		<div class="content__block-description">${homework.getContent()}</div>
		
		<div class="content__separator"></div>
		
		<h1 class="title">${homeworkAnswerTitle}</h1>
		
		<%--<c:forEach var="each" items="${homeworksList}">--%>
			<!--<div class="content__block">
				<h3 class="content__block-title">
                    <a class="content__block-title-link" href="homeworkView.action?homework_id=${each.getId()}">${each.getTitle()}</a>
                </h3>
				<div class="content__block-header">
					<div class="content__block-header-item content__block-header-item--date">${each.getFormattedCreationDate()}</div>
				</div>
				<div class="content__block-description">${each.getContent()}</div>
                <div class="content__block-footer">
                    <a class="button button--default" href="homeworkAddAnswer.action?homework_id=${each.getId()}">${homeworkAddAnswerLabel}</a>
                </div>
			</div>-->
		<%--</c:forEach>--%>
	
	</div>
	
	<%@ include file="../custom/aside.jsp"%>

</div>

<%@ include file="../custom/foot.jsp"%>