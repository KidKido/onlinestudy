<%@ include file="../custom/head.jsp"%>
<%@ include file="../custom/header.jsp"%>

<div class="container">
	
	<div class="content">
		<div class="content__header">
			<h1 class="title">${pageTitle}</h1>
			<a class="button button--default" href="lessonAdd.action" title="${lessonAddLabel}">&#x2b;</a>
		</div>
		<c:if test="${lessonsList.size() == 0}">
			<div class="content__block">
				<div class="content__block-description">${lessonsListEmptyLabel}</div>
			</div>
		</c:if>
		<c:forEach var="each" items="${lessonsList}">
			<div class="content__block">
				<h3 class="content__block-title">
					<a class="content__block-title-link" href="lessonView.action?lesson_id=${each.getId()}">${each.getTitle()}</a>
					<span class="content__block-title-description">${each.getSubject().getName()}</span>
				</h3>
				<div class="content__block-header">
					<div class="content__block-header-item content__block-header-item--date">${each.getFormattedCreationDate()}</div>
					<div class="content__block-header-item content__block-header-item--user">${each.getTeacher().getName()}</div>
					<div class="content__block-header-item content__block-header-item--tasks">${each.getHomeworkAmount()}</div>
				</div>
				<div class="content__block-description">${each.getShortContent()}</div>
			</div>
		</c:forEach>
		
		<%@ include file="../custom/pagination.jsp"%>
		
	</div>
	
	<%@ include file="../custom/aside.jsp"%>

</div>

<%@ include file="../custom/foot.jsp"%>