<%@ include file="../custom/head.jsp"%>
<%@ include file="../custom/header.jsp"%>

<div class="container">
	
	<div class="content">
		<div class="content__header">
			<h1 class="title">${pageTitle}</h1>
		</div>
		<c:forEach var="each" items="${homeworksList}">
			<div class="content__block">
				<h3 class="content__block-title">
					<a class="content__block-title-link" href="homeworkView.action?homework_id=${each.getId()}">${each.getLesson().getTitle()}: ${each.getTitle()}</a>
					<span class="content__block-title-description">${each.getLesson().getSubject().getName()}</span>
				</h3>
				<div class="content__block-header">
					<div class="content__block-header-item content__block-header-item--date">${each.getFormattedCreationDate()}</div>
				</div>
				<div class="content__block-description">${each.getContent()}</div>
			</div>
		</c:forEach>
		
		<%@ include file="../custom/pagination.jsp"%>
		
	</div>
	
	<%@ include file="../custom/aside.jsp"%>

</div>

<%@ include file="../custom/foot.jsp"%>