package com.onlinestudy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onlinestudy.dao.TeacherSubjectDao;
import com.onlinestudy.entities.TeacherSubject;

@Service
public class TeacherSubjectService {

	@Autowired(required = false)
	private TeacherSubjectDao teacherSubjectDao;
	
	@Transactional
	public Boolean hasSubject(Integer subjectId, Integer teacherId) {
		return teacherSubjectDao.hasSubject(subjectId, teacherId);
	}

	@Transactional
	public Boolean save(TeacherSubject teacherSubject) {
		return teacherSubjectDao.save(teacherSubject);
	}
}
