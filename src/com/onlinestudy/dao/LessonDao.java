package com.onlinestudy.dao;

import java.io.Serializable;
import java.util.List;
import java.util.function.IntBinaryOperator;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.onlinestudy.entities.Lesson;

@Repository
@SuppressWarnings("unchecked")
public class LessonDao {
	@Autowired
	private SessionFactory sessionFactory;

	public List<Lesson> getLessonsList(Integer limit, Integer offset, Integer subjectId, Integer teacherId) {
		String sql = "SELECT * "
				   + "FROM lesson "
				   + "WHERE lesson.teacher_id = '" + teacherId + "' ";
		
		if (subjectId > 0)
		{
			sql += " AND lesson.subject_id = '" + subjectId + "' ";
		}
		
		sql += "ORDER BY lesson.creation_date DESC "
		     + "LIMIT " + limit + ", " + offset;
		
		return sessionFactory.getCurrentSession().createSQLQuery(sql).addEntity(Lesson.class).list();
	}

	public Boolean save(Lesson lesson) {
		Serializable result = sessionFactory.getCurrentSession().save(lesson);
		sessionFactory.getCurrentSession().flush();
		
		return (result != null);
	}

	public Lesson getLesson(Integer lessonId) {
		String sql = "SELECT * FROM lesson WHERE id = '" + lessonId + "' LIMIT 1";

		return (Lesson)sessionFactory.getCurrentSession().createSQLQuery(sql).addEntity(Lesson.class).uniqueResult();
	}
	
	public Integer getHomeworkAmount(Integer lessonId) {
		String sql = "SELECT COUNT(*) AS amount "
				   + "FROM homework "
				   + "WHERE homework.lesson_id = '" + lessonId + "'";

		Number number = (Number) sessionFactory.getCurrentSession().createSQLQuery(sql).uniqueResult();

		return number.intValue();
	}
}
