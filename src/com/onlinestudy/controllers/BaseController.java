package com.onlinestudy.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import com.onlinestudy.entities.User;
import com.onlinestudy.services.SubjectService;
import com.onlinestudy.services.UserService;

public class BaseController {

	@Autowired(required = false)
	protected ModelAndView mav;
	
	@Autowired(required = false)
	private UserService userService;
	
	@Autowired(required = false)
	private SubjectService subjectService;

	public BaseController() {
		mav = new ModelAndView();
	}
	
	public void initialize(String uuid) {
		
		if (uuid != null) {
			User user = userService.getUser(uuid);
			
			mav.addObject("userName", user.getName());
			mav.addObject("userGender", user.getGender());
			mav.addObject("navHomeworksAmount", userService.getHomeworkAmount(uuid));
			
			mav.addObject("asideTitle", "Мої предмети");
			
			if (user.getUserGroupId() == 2) {
				mav.addObject("asideLinksList", subjectService.getTeacherSubjectsList(user.getId()));
			} else if (user.getUserGroupId() == 3) {
				mav.addObject("asideLinksList", subjectService.getStudentSubjectsList(user.getId()));
			}
			
		}

		mav.addObject("userNavProfile", "Профіль");
		mav.addObject("userNavLogout", "Вийти");

		mav.addObject("navLessons", "Лекції");
		mav.addObject("navHomeworks", "Домашні завдання");
		
		mav.addObject("chooseOption", "Оберіть зі списку");
	}
}
