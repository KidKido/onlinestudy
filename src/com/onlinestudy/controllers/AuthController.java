package com.onlinestudy.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlinestudy.services.AuthService;

@Controller
public class AuthController extends BaseController {

	@Autowired(required = false)
	AuthService authService;

	@RequestMapping(value = "/index.action", method = RequestMethod.GET)
	public ModelAndView getIndex(HttpServletRequest request) {

		if (request.getCookies() != null && authService.isLoggedIn(request.getCookies())) {
				return new ModelAndView("redirect:lesson.action");
		}

		mav.setViewName("auth/login");

		mav.addObject("pageTitle", "Авторизація");
		mav.addObject("title", "Логін");
		mav.addObject("loginLabel", "Електронна пошта");
		mav.addObject("loginPlaceholder", "Введіть E-mail");
		mav.addObject("passwordLabel", "Пароль");
		mav.addObject("passwordPlaceholder", "Введіть пароль");
		mav.addObject("loginButton", "Ввійти");
		mav.addObject("registerButton", "Зареєструватися");

		return mav;
	}

	@RequestMapping(value = "/authCheckLogin.action", method = RequestMethod.POST)
	@ResponseBody
	public String postCheckLogin(@RequestParam(name = "email", required = false) String email,
			@RequestParam(name = "password", required = false) String password, HttpServletResponse response)
					throws JsonProcessingException {

		List<String> errorsList = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();

		if (email == "") {
			errorsList.add("Поле електронна пошта має бути заповнене.");
		}

		if (password == "") {
			errorsList.add("Поле пароль має бути заповнене.");
		}

		if (email != "" && password != "") {
			if (!authService.checkLogin(email, password)) {
				errorsList.add("Некоректна електронна пошта або пароль.");
			} else {
				response.addCookie(authService.login(email, password));
			}
		}

		return mapper.writeValueAsString(errorsList);
	}

	@RequestMapping(value = "/authLogout.action", method = RequestMethod.GET)
	public ModelAndView getLogout(HttpServletRequest request, HttpServletResponse response) {
		if (authService.isLoggedIn(request.getCookies())) {
			response.addCookie(authService.logout(request.getCookies()));
		}

		return new ModelAndView("redirect:index.action");
	}
}
