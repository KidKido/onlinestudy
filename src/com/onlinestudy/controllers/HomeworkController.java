package com.onlinestudy.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlinestudy.entities.Homework;
import com.onlinestudy.entities.Lesson;
import com.onlinestudy.entities.User;
import com.onlinestudy.services.AuthService;
import com.onlinestudy.services.HomeworkService;
import com.onlinestudy.services.LessonService;
import com.onlinestudy.services.PaginationService;
import com.onlinestudy.services.UserService;

@Controller
public class HomeworkController extends BaseController {

	@Autowired(required = false)
	private PaginationService paginationService;

	final private static int LIMIT = 5;
	final private static String TABLE = "homework";

	@Autowired(required = false)
	private HomeworkService homeworkService;

	@Autowired(required = false)
	AuthService authService;
	
	@Autowired(required = false)
	private UserService userService;
	
	@Autowired(required = false)
	private LessonService lessonService;

	@RequestMapping(value = "/homework.action", method = RequestMethod.GET)
	public ModelAndView getHomework(HttpServletRequest request,
			@CookieValue(value = "uuid", required = false) String uuid,
			@RequestParam(value = "page", required = false, defaultValue = "1") Integer page) {
		
		if (!authService.isLoggedIn(request.getCookies())) {
			return new ModelAndView("redirect:index.action");
		}

		super.initialize(uuid);

		mav.setViewName("homework/homework");
		
		paginationService.initialize();
		
		paginationService.setLink("homework.action");
		paginationService.setCurrent(page);
		paginationService.setLimit(LIMIT);
		paginationService.setTable(TABLE);
		paginationService.run();		

		mav.addObject("pagination", paginationService);
		mav.addObject("pageTitle", "Домашні завдання");
		mav.addObject("homeworksList", homeworkService.getHomeworksList(paginationService.getLimit(), paginationService.getOffset(), userService.getUser(uuid).getId()));

		return mav;
	}
	
	@RequestMapping(value = "/homeworkAdd.action", method = RequestMethod.GET)
	public ModelAndView getAddSubject(@CookieValue(value = "uuid", required = false) String uuid,
			@RequestParam(value = "lesson_id", required = false) Integer lessonId,
			HttpServletRequest request) {

		if (!authService.isLoggedIn(request.getCookies())) {
			return new ModelAndView("redirect:index.action");
		}

		super.initialize(uuid);

		mav.setViewName("homework/add");

		mav.addObject("pageTitle", "Додавання домашнього завдання");
		mav.addObject("lessonId", lessonId);
		mav.addObject("titleLabel", "Назва домашнього завдання");
		mav.addObject("titlePlaceholder", "Домашнє завдання №1");
		mav.addObject("contentLabel", "Зміст завдання");
		mav.addObject("contentPlaceholder", "Зміст завдання...");
		mav.addObject("saveButton", "Зберегти");
		mav.addObject("cancelButton", "Скасувати");

		return mav;
	}
	
	@RequestMapping(value = "/homeworkAddCheck.action", method = RequestMethod.POST)
	@ResponseBody
	public String postHomeworkChooseCheck(@CookieValue(value = "uuid", required = false) String uuid,
			@RequestParam(value = "lesson", required = false) Integer lessonId, 
			@RequestParam(value = "title", required = false) String title, 
			@RequestParam(value = "content", required = false) String content) throws JsonProcessingException {

		List<String> errorsList = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();

		if (title == "") {
			errorsList.add("Введіть назву домашнього завдання.");
		}
		
		if (content == "") {
			errorsList.add("Введіть зміст домашнього завдання.");
		} 
		
		if (errorsList.isEmpty()) {
			Homework homework = new Homework();

			User teacher = new User();
			teacher.setId(userService.getUser(uuid).getId());
			
			Lesson lesson = new Lesson();
			lesson.setId(lessonService.getLesson(lessonId).getId());

			homework.setTeacher(teacher);
			homework.setLesson(lesson);
			homework.setCreationDate(new Date());
			homework.setTitle(title);
			homework.setContent(content);

			if (!homeworkService.save(homework)) {
				errorsList.add("Помилка додавання домашнього завдання.");
			}
		}

		return mapper.writeValueAsString(errorsList);
	}

	@RequestMapping(value = "/homeworkView.action", method = RequestMethod.GET)
	public ModelAndView getLessonView(@CookieValue(value = "uuid", required = false) String uuid,
									  @RequestParam(value = "homework_id", required = false) Integer homeworkId,
									  HttpServletRequest request) {

		if (!authService.isLoggedIn(request.getCookies())) {
			return new ModelAndView("redirect:index.action");
		}

		if (homeworkId == null) {
			return new ModelAndView("redirect:lesson.action");
		}

		super.initialize(uuid);

		mav.setViewName("homework/view");

		Homework homework = homeworkService.getHomework(homeworkId);

		mav.addObject("pageTitle", homework.getTitle());
		mav.addObject("homework", homework);
		mav.addObject("homeworkAnswerTitle", "Відповіді");

		return mav;
	}

}
