package com.onlinestudy.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.onlinestudy.entities.Homework;

@Repository
@SuppressWarnings("unchecked")
public class HomeworkDao {
	@Autowired
	private SessionFactory sessionFactory;

	public List<Homework> getHomeworksList(Integer limit, Integer offset, Integer teacherId) {
		String sql = "SELECT * "
				   + "FROM homework "
				   + "WHERE homework.teacher_id = '" + teacherId + "'"
				   + "ORDER BY homework.creation_date DESC "
				   + "LIMIT " + limit + ", " + offset;
		
		return sessionFactory.getCurrentSession().createSQLQuery(sql).addEntity(Homework.class).list();
	}
	
	public Boolean save(Homework homework) {
		Serializable result = sessionFactory.getCurrentSession().save(homework);
		sessionFactory.getCurrentSession().flush();
		
		return (result != null);
	}

	public List<Homework> getHomeworksList(Integer lessonId) {
		List<Homework> homeworksList = sessionFactory.getCurrentSession().createCriteria(Homework.class)
				.addOrder(Order.desc("creationDate")).add(Restrictions.eq("lessonId", lessonId)).list();

		return homeworksList;
	}

	public Homework getHomework(Integer homeworkId) {
		String sql = "SELECT * FROM homework WHERE id = '" + homeworkId + "' LIMIT 1";

		return (Homework) sessionFactory.getCurrentSession().createSQLQuery(sql).addEntity(Homework.class).uniqueResult();
	}
}
