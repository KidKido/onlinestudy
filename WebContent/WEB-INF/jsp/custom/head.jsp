<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
	<head>
		<title>${pageTitle} | OnlineStudy</title>
		<meta charset="utf-8" />
		<meta name="robots" content="noindex, nofollow" />
		<link rel="stylesheet" href="styles/reset.css" />
		<link rel="stylesheet" href="styles/form.css" />
		<link rel="stylesheet" href="styles/custom.css" />
	</head>
	<body>