package com.onlinestudy.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onlinestudy.dao.SubjectDao;
import com.onlinestudy.entities.Subject;

@Service
public class SubjectService {

	@Autowired(required = false)
	private SubjectDao subjectDao;

	@Transactional
	public List<Subject> getSubjectsList() {

		return subjectDao.getSubjectsList();
	}
	
	@Transactional
	public List<Subject> getTeacherSubjectsList(Integer teacherId) {

		return subjectDao.getTeacherSubjectsList(teacherId);
	}
	
	@Transactional
	public List<Subject> getStudentSubjectsList(Integer studentId) {

		return subjectDao.getStudentSubjectsList(studentId);
	}

	@Transactional
	public Boolean getSubject(String name) {

		return subjectDao.getSubject(name);
	}

	@Transactional
	public Boolean addSubject(Subject subject) {

		return subjectDao.addSubject(subject);
	}

}
