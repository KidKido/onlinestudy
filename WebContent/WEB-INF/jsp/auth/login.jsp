<%@ include file="../custom/head.jsp"%>

<form class="login-form" method="post" action="signin.action">

	<h1 class="login-form__title">${title}</h1>
	
	<div class="login-form__errors"></div>
	
	<label class="label login-form__label" for="email">${loginLabel}</label>
	<input class="input login-form__input login-form__email" id="email" type="text" name="email" placeholder="${loginPlaceholder}" />
	
	<label class="label login-form__label" for="password">${passwordLabel}</label>
	<input class="input login-form__input login-form__password" id="password" type="password" name="password" placeholder="${passwordPlaceholder}" />
	
	<input class="button button--primary login-form__submit" type="submit" value="${loginButton}" />
	<a class="button button--link login-form__link" href="registration.action">${registerButton}</a>
	
</form>

<%@ include file="../custom/foot.jsp"%>