package com.onlinestudy.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlinestudy.dao.PaginationDao;

@Service
public class PaginationService {

	final private static int LEFT_RIGHT_PAGES_AMOUNT = 3;

	@Autowired
	private PaginationDao paginationDao;

	private String table;
	private String filter;
	private String link;
	private List<String> query;
	private Integer current;
	private Integer total;
	private Integer limit;
	private Integer offset;
	private List<Integer> list;

	public Integer getCurrent() {
		return current;
	}

	public void setCurrent(Integer current) {
		this.current = current;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getTotal() {
		return total;
	}

	public Integer getOffset() {
		return offset;
	}

	public List<Integer> getList() {
		return list;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getQueryString() {
		return String.join("&", query);
	}

	public void setQueryString(String key, String value) {
		this.query.add(key + '=' + value);
	}

	public void initialize() {
		list = new ArrayList<>();
		query = new ArrayList<>();
		current = 1;
		limit = 0;
		link = null;
		table = null;
		filter = null;
	}
	
	public void run() {
		total = (int) Math.ceil(paginationDao.getTotalAmount(table, filter) * 1.0 / limit);
		offset = limit;
		limit = 0;

		if (current > 1) {
			if (current > total) {
				current = total;
			}

			limit = (current - 1) * offset;
		}

		Integer start = 1;
		Integer end = total;

		if (current - LEFT_RIGHT_PAGES_AMOUNT > 0) {
			start = current - LEFT_RIGHT_PAGES_AMOUNT;
		}

		if (current + LEFT_RIGHT_PAGES_AMOUNT <= total) {
			end = current + LEFT_RIGHT_PAGES_AMOUNT;
		}

		for (int i = start; i <= end; i++) {
			list.add(i);
		}
	}
}
