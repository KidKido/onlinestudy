<%@ include file="../custom/head.jsp"%>
<%@ include file="../custom/header.jsp"%>

<div class="container">
	
	<div class="content">
		<h1 class="title">${pageTitle}</h1>
		<form class="content__form homework-form--add" method="post" action="homeworkAddCheck.action">
		
			<div class="content__form-errors"></div>
		
			<div class="content__form-row">
				<label class="label" for="title">${titleLabel}</label>
				<input class="input" id="title" type="text" name="title" placeholder="${titlePlaceholder}" data-required="true" />
			</div>
			
			<div class="content__form-row">
				<label class="label" for="content">${contentLabel}</label>
				<textarea class="input input--textarea" id="content" name="content" placeholder="${contentPlaceholder}" data-required="true"></textarea>
			</div>
			
			<div class="content__form-row">
				<input class="hidden" id="lesson" type="hidden" name="lesson" value="${lessonId}" />
				<input class="button button--primary homework-form--add__submit" type="submit" value="${saveButton}" />
				<a class="button button--link" href="lessonView.action?lesson_id=${lessonId}">${cancelButton}</a>
			</div>
			
		</form>
	</div>
	
	<%@ include file="../custom/aside.jsp"%>

</div>

<%@ include file="../custom/foot.jsp"%>