<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="aside">
	<div class="aside__title">
		<h2 class="title">${asideTitle}</h2>
		<a class="button button--default" href="subjectChoose.action">&#x2b;</a>
	</div>
	<ul class="aside__list">
		<c:forEach var="each" items="${asideLinksList}">
			<li class="aside__list-item">
				<a class="aside__link" href="lesson.action?subject_id=${each.getId()}">${each.getName()}</a>
			</li>
		</c:forEach>
	</ul>
</div>