<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${pagination.getList().size() > 1}">
	<div class="pagination">
		<c:if test="${pagination.getCurrent() != 1}">
			<a class="pagination__item" href="${pagination.getLink()}?${pagination.getQueryString()}&page=1">&#x276e;</a>
		</c:if>
		<c:forEach var="each" items="${pagination.getList()}">
			<c:if test="${each == pagination.getCurrent()}">
				<a class="pagination__item pagination__item--current" href="#">${each}</a>
			</c:if>
			<c:if test="${each != pagination.getCurrent()}">
				<a class="pagination__item" href="${pagination.getLink()}?${pagination.getQueryString()}&page=${each}">${each}</a>
			</c:if>
		</c:forEach>
		<c:if test="${pagination.getCurrent() != pagination.getTotal()}">
			<a class="pagination__item" href="${pagination.getLink()}?${pagination.getQueryString()}&page=${pagination.getTotal()}">&#x276f;</a>
		</c:if>
	</div>
</c:if>