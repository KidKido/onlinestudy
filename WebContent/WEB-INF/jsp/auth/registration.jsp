<%@ include file="../custom/head.jsp"%>

<div class="container container--without-header">
	
	<div class="content">
		<h1 class="title">${pageTitle}</h1>
		<form class="content__form registration-form" method="post" action="registrationCheck.action">
			
			<div class="content__form-errors"></div>
		
			<div class="content__form-row">
				<label class="label" for="group">${groupLabel}</label>
				<select class="input input--select" id="group" name="group" data-required="true">
					<option value="">${chooseOption}</option>
					<option value="2">${groupTeacherOption}</option>
					<option value="3">${groupStudentOption}</option>
				</select>
			</div>
			<div class="content__form-row">
				<label class="label" for="email">${emailLabel}</label>
				<input class="input" id="email" type="text" name="email" placeholder="${emailPlaceholder}" data-required="true" />
			</div>
			<div class="content__form-row">
				<label class="label" for="password">${passwordLabel}</label>
				<input class="input" id="password" type="password" name="password" data-required="true" />
			</div>
			<div class="content__form-row">
				<label class="label" for="passwordRepeat">${passwordRepeatLabel}</label>
				<input class="input" id="passwordRepeat" type="password" name="passwordRepeat" data-required="true" />
			</div>
			<div class="content__form-row">
				<label class="label" for="name">${nameLabel}</label>
				<input class="input" id="name" type="text" name="name" placeholder="${namePlaceholder}" data-required="true" />
			</div>
			<div class="content__form-row">
				<label class="label" for="gender">${genderLabel}</label>
				<select class="input input--select" id="gender" name="gender" data-required="true">
					<option value="">${chooseOption}</option>
					<option value="0">${genderFemaleOption}</option>
					<option value="1">${genderMaleOption}</option>
				</select>
			</div>
			<div class="content__form-row">
				<label class="label" for="birthDate">${birthDateLabel}</label>
				<input class="input datepicker" id="birthDate" type="text" name="birthDate" placeholder="${birthDatePlaceholder}" data-required="true" />
			</div>
			<div class="content__form-row">
				<label class="label" for="region">${regionLabel}</label>
				<select class="input input--select registration__region" id="region" name="region" data-required="true">
					<option value="">${chooseOption}</option>
					<c:forEach var="each" items="${regionsList}">
						<option value="${each.getId()}">${each.getName()}</option>
					</c:forEach>
				</select>
			</div>
			<div class="content__form-row hidden">
				<label class="label" for="city">${cityLabel}</label>
				<select class="input input--select registration__city" id="city" name="city" data-required="true">
					<option value="">${chooseOption}</option>
				</select>
			</div>
			<div class="content__form-row">
				<input class="button button--primary registration-form__submit" type="submit" value="${registerButton}" />
				<a class="button button--link" href="index.action">${cancelButton}</a>
			</div>
		</form>
	</div>
	
	<div class="aside">
		<h2 class="title">${informationTitle}</h2>
		<div class="aside__text">${informationText}</div>
	</div>

</div>

<%@ include file="../custom/foot.jsp"%>