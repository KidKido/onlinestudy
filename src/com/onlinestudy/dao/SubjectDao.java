package com.onlinestudy.dao;

import java.io.Serializable;
import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.onlinestudy.entities.Subject;

@Repository
@SuppressWarnings("unchecked")
public class SubjectDao {

	@Autowired
	private SessionFactory sessionFactory;

	public List<Subject> getSubjectsList() {
		
		return sessionFactory.getCurrentSession().createCriteria(Subject.class)
				.addOrder(Order.asc("name")).list();
	}
	
	public List<Subject> getTeacherSubjectsList(Integer teacherId) {
		
		String sql = "SELECT subject.id AS id, subject.name AS name "
				   + "FROM teacher_subject "
				   + "LEFT JOIN subject "
				   + "ON subject.id = teacher_subject.subject_id "
				   + "WHERE teacher_subject.teacher_id = '" + teacherId + "' "
				   + "ORDER BY subject.name ASC";
		
		return sessionFactory.getCurrentSession().createSQLQuery(sql).addEntity(Subject.class).list();
	}
	
	public List<Subject> getStudentSubjectsList(Integer studentId) {
		
		String sql = "SELECT DISTINCT subject.name "
				   + "FROM student_subject "
				   + "LEFT JOIN subject "
				   + "ON subject.id = student_subject.subject_id "
				   + "WHERE student_subject.student_id = '" + studentId + "' "
				   + "ORDER BY subject.name ASC";
		
		return sessionFactory.getCurrentSession().createSQLQuery(sql).addEntity(Subject.class).list();
	}

	public Boolean getSubject(String name) {
		
		String sql = "SELECT name FROM subject WHERE name = '" + name + "' LIMIT 1";
		Object getSubject = sessionFactory.getCurrentSession().createSQLQuery(sql).uniqueResult();
		
		return (getSubject != null);
	}

	public Boolean addSubject(Subject subject) {

		Serializable result = sessionFactory.getCurrentSession().save(subject);
		sessionFactory.getCurrentSession().flush();

		return (result != null);
	}

}
