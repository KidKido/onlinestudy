package com.onlinestudy.services;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onlinestudy.dao.LessonDao;
import com.onlinestudy.entities.Lesson;

@Service
public class LessonService {

	@Autowired
	private LessonDao lessonDao;

	@Transactional
	public List<Lesson> getLessonsList(Integer limit, Integer offset, Integer subjectId, Integer teacherId) {
		List<Lesson> lessonsList = lessonDao.getLessonsList(limit, offset, subjectId, teacherId);
		
		for (Lesson each : lessonsList) {
			String[] string = Jsoup.parse(each.getContent()).text().split("[\\s+]");
			
			if (string.length > 35) {
				each.setShortContent(String.join(" ", Arrays.copyOfRange(string, 0, 35)) + "...");
			} else {
				each.setShortContent(String.join(" ", string));
			}
			
			each.setFormattedCreationDate(new SimpleDateFormat("dd MMMM yyyy о HH:mm", new Locale("uk", "UA")).format(each.getCreationDate()));
			each.setHomeworkAmount(lessonDao.getHomeworkAmount(each.getId()));
		}
		
		return lessonsList;
	}

	@Transactional
	public Boolean save(Lesson lesson) {
		return lessonDao.save(lesson);
	}

	@Transactional
	public Lesson getLesson(Integer lessonId) {
		Lesson lesson = lessonDao.getLesson(lessonId);
		
		lesson.setFormattedCreationDate(new SimpleDateFormat("dd MMMM yyyy о HH:mm", new Locale("uk", "UA")).format(lesson.getCreationDate()));
		lesson.setHomeworkAmount(lessonDao.getHomeworkAmount(lesson.getId()));
		
		return lesson;
	}
}
