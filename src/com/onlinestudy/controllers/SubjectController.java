package com.onlinestudy.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlinestudy.entities.Subject;
import com.onlinestudy.entities.TeacherSubject;
import com.onlinestudy.entities.User;
import com.onlinestudy.services.AuthService;
import com.onlinestudy.services.SubjectService;
import com.onlinestudy.services.TeacherSubjectService;
import com.onlinestudy.services.UserService;

@Controller
public class SubjectController extends BaseController {

	@Autowired(required = false)
	private SubjectService subjectService;

	@Autowired(required = false)
	private UserService userService;

	@Autowired(required = false)
	AuthService authService;

	@Autowired(required = false)
	TeacherSubjectService teacherSubjectService;

	@RequestMapping(value = "/subjectChoose.action", method = RequestMethod.GET)
	public ModelAndView getSubjectChoose(HttpServletRequest request,
			@CookieValue(value = "uuid", required = false) String uuid) {

		if (!authService.isLoggedIn(request.getCookies())) {
			return new ModelAndView("redirect:index.action");
		}

		super.initialize(uuid);

		mav.setViewName("subject/choose");

		mav.addObject("pageTitle", "Список предметів");

		mav.addObject("subjectLabel", "Предмет");
		mav.addObject("subjectsList", subjectService.getSubjectsList());

		mav.addObject("addButton", "Додати новий предмет");
		mav.addObject("saveButton", "Зберегти обраний предмет");

		mav.addObject("user", userService.getUser(uuid));

		return mav;
	}

	@RequestMapping(value = "/subjectChooseCheck.action", method = RequestMethod.POST)
	@ResponseBody
	public String postSubjectChooseCheck(@CookieValue(value = "uuid", required = false) String uuid,
			@RequestParam(value = "subject", required = false) Integer subjectId) throws JsonProcessingException {

		List<String> errorsList = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();

		if (subjectId == null) {
			errorsList.add("Оберіть предмет.");
		} else if (teacherSubjectService.hasSubject(subjectId, userService.getUser(uuid).getId())) {
			errorsList.add("Обраний предмет вже є у вашому списку.");
		}

		if (errorsList.isEmpty()) {
			TeacherSubject teacherSubject = new TeacherSubject();

			User teacher = new User();
			teacher.setId(userService.getUser(uuid).getId());
			teacherSubject.setTeacher(teacher);

			Subject subject = new Subject();
			subject.setId(subjectId);
			teacherSubject.setSubject(subject);

			if (!teacherSubjectService.save(teacherSubject)) {
				errorsList.add("Помилка додавання предмету.");
			}
		}

		return mapper.writeValueAsString(errorsList);
	}

	@RequestMapping(value = "/subjectAdd.action", method = RequestMethod.GET)
	public ModelAndView getAddSubject(@CookieValue(value = "uuid", required = false) String uuid,
			HttpServletRequest request) {

		if (!authService.isLoggedIn(request.getCookies())) {
			return new ModelAndView("redirect:index.action");
		}

		super.initialize(uuid);

		mav.setViewName("subject/add");

		mav.addObject("pageTitle", "Додавання предмету");
		mav.addObject("subjectLabel", "Назва предмету");
		mav.addObject("subjectPlaceholder", "Наприклад: Англійська мова");
		mav.addObject("saveButton", "Зберегти");
		mav.addObject("cancelButton", "Скасувати");

		return mav;

	}

	@RequestMapping(value = "/subjectAddCheck.action", method = RequestMethod.POST)
	@ResponseBody
	public String postAddSubjcetCheck(@RequestParam(value = "subject", required = false) String name)
			throws JsonProcessingException {
		List<String> errorsList = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();

		if (name == null) {
			errorsList.add("Введіть назву предмету.");
		} else if (subjectService.getSubject(name)) {
			errorsList.add("Такий предмет вже існує в базі.");
		}

		if (errorsList.isEmpty()) {
			Subject subject = new Subject();

			subject.setName(name);

			if (!subjectService.addSubject(subject)) {
				errorsList.add("Помилка додавання предмету.");
			}
		}

		return mapper.writeValueAsString(errorsList);

	}
}
