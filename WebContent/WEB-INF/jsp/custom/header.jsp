<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="header">

	<div class="header__container">

		<div class="header__logo">
			<a class="header__logo-link" href="lesson.action">
				<img class="header__logo-image" src="images/logo.png" alt="OnlineStudy" />
			</a>
		</div>
		
		<div class="header__user">
			
				<div class="header__user-name
					<c:if test="${userGender == 0}">header__user-name--female</c:if>
					<c:if test="${userGender == 1}">header__user-name--male</c:if>">${userName}</div>
					
				<ul class="header__user-menu">
					<!--  <li class="header__user-menu-item">
						<a class="header__user-menu-link" href="user.action">${userNavProfile}</a>
					</li>-->
					<li class="header__user-menu-item">
						<a class="header__user-menu-link" href="authLogout.action">${userNavLogout}</a>
					</li>
				</ul>
					
		</div>
		
		<ul class="header__menu">
			<li class="header__menu-item">
				<a class="header__menu-link" href="lesson.action">
					<span class="header__menu-link-name">${navLessons}</span>
				</a>
			</li>
			<li class="header__menu-item">
				<a class="header__menu-link" href="homework.action">
					<span class="header__menu-link-name">${navHomeworks}</span>
					<c:if test="${navHomeworksAmount > 0}">
						<span class="header__menu-link-amount">${navHomeworksAmount}</span>
					</c:if>
				</a>
			</li>
		</ul>
		
	</div>

</div>