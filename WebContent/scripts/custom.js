/**
 * Defaults
 */
Global.Class = {
	ACTIVE: 'active',
	ERROR:  'error'
};

Global.Time = {
	ANIMATION: 200
};

function Global()
{
	
}

$('.datepicker').datepicker(
{
	dateFormat: 'dd.mm.yy',
	prevText: '&#x276e;',
	nextText: '&#x276f;',
	changeMonth: true,
	monthNamesShort: ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
	changeYear: true,
	yearRange: 'c-120:c',
	firstDay: 0,
	dayNamesMin: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Нд']
});

/**
 * Function
 */
function showErrors(object, errors)
{
	if (!errors.length)
	{
		object.html('').hide();
		
		return false;
	}
	
	var content = '<ul class="errors-list">';
	
	for (var i = 0, n = errors.length; i < n; i++)
	{
		content += '<li class="errors-list__item">' + errors[i] + '</li>';
	}
	
	content += '</ul>';
	
	object.html(content).fadeIn(Global.Time.ANIMATION);
	$('html, body').animate({ scrollTop: 0 }, Global.Time.ANIMATION);
}

function convertQueryToArray(query)
{
	var result = {},
		temp = [];
	
	query = query.split('&');
	
	for (var i = 0, n = query.length; i < n; i++)
	{
		temp = query[i].split('=');
		temp[0] = decodeURIComponent(temp[0]);
		temp[0] = temp[0].replace(/\+/g, ' ');
		temp[1] = decodeURIComponent(temp[1]);
		temp[1] = temp[1].replace(/\+/g, ' ');
		
		result[temp[0]] = temp[1];
	}
	
	return result;
}

function submitForm(form, submit, errors, sourceLink, destinationLink)
{
	submit.prop('disabled', true);
	
	$.post(sourceLink, convertQueryToArray(form.serialize()),
	function (errorsList)
	{
		if (errorsList.length)
		{
			showErrors(errors, errorsList);
			
			submit.prop('disabled', false);
		}
		else
		{
			location.href = destinationLink;
		}
	}, 'json');
}

/**
 * Header
 */
var headerUser = $('.header__user'),
	headerUserMenu = $('.header__user-menu');

headerUser.on('click', function ()
{
	if (headerUser.hasClass(Global.Class.ACTIVE))
	{
		headerUserMenu.slideUp(0, function ()
		{
			headerUser.removeClass(Global.Class.ACTIVE);
		});
	}
	else
	{
		headerUser.addClass(Global.Class.ACTIVE);
		headerUserMenu.slideDown(Global.Time.ANIMATION);
	}
});

/**
 * Login
 */
var loginForm = $('.login-form'),
	loginErrors = $('.login-form__errors'),
	loginSubmit = $('.login-form__submit');

loginForm.on('submit', function (event)
{
	event.preventDefault();
	
	submitForm(loginForm, loginSubmit, loginErrors, 'authCheckLogin.action', 'lesson.action');
	
	return false;
});

/**
 * Registration
 */
// Get cities list
var registrationRegion = $('.registration__region'),
	registrationCity = $('.registration__city');

registrationRegion.on('change', function ()
{
	var regionId = $(this).val();
	
	if (!regionId)
	{
		registrationCity.parent().hide();
		
		return false;
	}
	
	$.post('locationGetCitiesList.action',
	{
		regionId: regionId
	},
	function (citiesList)
	{
		var cities = '';
			
		for (var i = 0, n = citiesList.length; i < n; i++)
		{
			cities += '<option value="' + citiesList[i].id + '">' + citiesList[i].name + '</option>';
		}
		
		registrationCity.find('option:not(:first-child)').remove();
		registrationCity.append(cities);
		registrationCity.parent().fadeIn(Global.Time.ANIMATION);
	}, 'json');
});

// Form submit
var registrationForm = $('.registration-form'),
	registrationErrors = $('.content__form-errors'),
	registrationSubmit = $('.registration-form__submit');

registrationForm.on('submit', function (event)
{
	event.preventDefault();
	
	submitForm(registrationForm, registrationSubmit, registrationErrors, 'registrationCheck.action', 'lesson.action');
	
	return false;
});

/**
 * Subject
 */
// Add subject
var subjectAddForm = $('.subject-form--add'),
	subjectAddErrors = $('.content__form-errors'),
	subjectAddSubmit = $('.subject-form--add__submit');

subjectAddForm.on('submit', function (event)
{
	event.preventDefault();
	
	submitForm(subjectAddForm, subjectAddSubmit, subjectAddErrors, 'subjectAddCheck.action', 'lesson.action');
	
	return false;
});

// Add teacher subject
var subjectTeacherForm = $('.subject-form--teacher'),
	subjectTeacherErrors = $('.content__form-errors'),
	subjectTeacherSubmit = $('.subject-form--teacher__submit');

subjectTeacherForm.on('submit', function (event)
{
	event.preventDefault();
	
	submitForm(subjectTeacherForm, subjectTeacherSubmit, subjectTeacherErrors, 'subjectChooseCheck.action', 'lesson.action');
	
	return false;
});

/**
 * Lesson
 */
// Add lesson
var lessonAddForm = $('.lesson-form--add'),
	lessonAddErrors = $('.content__form-errors'),
	lessonAddSubmit = $('.lesson-form--add__submit');

lessonAddForm.on('submit', function (event)
{
	event.preventDefault();
	
	submitForm(lessonAddForm, lessonAddSubmit, lessonAddErrors, 'lessonAddCheck.action', 'lesson.action');
	
	return false;
});

/**
 * Homework
 */
// Add homework
var homeworkAddForm = $('.homework-form--add'),
	homeworkAddLesson = $('.homework-form--add #lesson').val(),
	homeworkAddErrors = $('.content__form-errors'),
	homeworkAddSubmit = $('.homework-form--add__submit');

homeworkAddForm.on('submit', function (event)
{
	event.preventDefault();
	
	submitForm(homeworkAddForm, homeworkAddSubmit, homeworkAddErrors, 'homeworkAddCheck.action', 'lessonView.action?lesson_id=' + homeworkAddLesson);
	
	return false;
});