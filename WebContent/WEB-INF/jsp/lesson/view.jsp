<%@ include file="../custom/head.jsp"%>
<%@ include file="../custom/header.jsp"%>

<div class="container">
	
	<div class="content">
		
		<div class="content__header">
			<h1 class="title">${pageTitle} <span class="content__block-title-description">${lesson.getSubject().getName()}</span></h1>
			<a class="button button--default" href="homeworkAdd.action?lesson_id=${lesson.getId()}" title="${homeworkAddLabel}">&#x2b;</a>
		</div>
		
		<div class="content__block-header">
			<div class="content__block-header-item content__block-header-item--date">${lesson.getFormattedCreationDate()}</div>
			<div class="content__block-header-item content__block-header-item--user">${lesson.getTeacher().getName()}</div>
			<div class="content__block-header-item content__block-header-item--tasks">${lesson.getHomeworkAmount()}</div>
		</div>
		
		<div class="content__block-description">${lesson.getContent()}</div>
		
		<div class="content__separator"></div>
		
		<h1 class="title">${homeworkTitle}</h1>
		
		<c:forEach var="each" items="${homeworksList}">
			<div class="content__block">
				<h3 class="content__block-title">
                    <a class="content__block-title-link" href="homeworkView.action?homework_id=${each.getId()}">${each.getTitle()}</a>
                </h3>
				<div class="content__block-header">
					<div class="content__block-header-item content__block-header-item--date">${each.getFormattedCreationDate()}</div>
				</div>
				<div class="content__block-description">${each.getContent()}</div>
                <div class="content__block-footer">
                    <a class="button button--default" href="homeworkAddAnswer.action?homework_id=${each.getId()}">${homeworkAddAnswerLabel}</a>
                </div>
			</div>
		</c:forEach>
	
	</div>
	
	<%@ include file="../custom/aside.jsp"%>

</div>

<%@ include file="../custom/foot.jsp"%>