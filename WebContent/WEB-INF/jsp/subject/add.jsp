<%@ include file="../custom/head.jsp"%>
<%@ include file="../custom/header.jsp"%>

<div class="container">
	
	<div class="content">
		<h1 class="title">${pageTitle}</h1>
		<form class="content__form subject-form--add" method="post" action="subjectAddCheck.action">
		
			<div class="content__form-errors"></div>
		
			<div class="content__form-row">
				<label class="label" for="subject">${subjectLabel}</label>
				<input class="input" id="subject" type="text" name="subject" placeholder="${subjectPlaceholder}" data-required="true" />
			</div>
			
			<div class="content__form-row">
				<input class="button button--primary subject-form--add__submit" type="submit" value="${saveButton}" />
				<a class="button button--link" href="lesson.action">${cancelButton}</a>
			</div>
			
		</form>
	</div>
	
	<%@ include file="../custom/aside.jsp"%>

</div>

<%@ include file="../custom/foot.jsp"%>