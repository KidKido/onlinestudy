package com.onlinestudy.dao;

import java.io.Serializable;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.onlinestudy.entities.User;

@Repository
public class UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Boolean saveUser(User user) {
		Serializable result = sessionFactory.getCurrentSession().save(user);
		sessionFactory.getCurrentSession().flush();

		return (result != null);
	}

	public User getUser(String uuid) {
		Object result = sessionFactory.getCurrentSession().createCriteria(User.class).add(Restrictions.eq("uuid", uuid))
				.uniqueResult();

		return (User) result;
	}

	public Integer getHomeworkAmount(String uuid) {
		String sql = "SELECT COUNT(*) AS amount " + 
					 "FROM homework, user " +
					 "WHERE user.uuid = '" + uuid + "' AND homework.teacher_id = user.id";

		Number number = (Number) sessionFactory.getCurrentSession().createSQLQuery(sql).uniqueResult();

		return number.intValue();
	}
}
