<%@ include file="../custom/head.jsp"%>
<%@ include file="../custom/header.jsp"%>

<div class="container">
	
	<div class="content">
	
		<h1 class="title">${pageTitle}</h1>
		
		<c:if test="${user.getUserGroupId() == 2}">
			<form class="content__form subject-form--teacher" method="post" action="subjectCheck.action">
			
				<div class="content__form-errors"></div>
			
				<div class="content__form-row">
					<label class="label" for="subject">${subjectLabel}</label>
					<select class="input input--select" id="subject" name="subject" data-required="true">
						<option value="">${chooseOption}</option>
						<c:forEach var="each" items="${subjectsList}">
							<option value="${each.getId()}">${each.getName()}</option>
						</c:forEach>
					</select>
				</div>
				
				<div class="content__form-row">
					<input class="button button--primary subject-form--teacher__submit" type="submit" value="${saveButton}" />
					<a class="button button--link" href="subjectAdd.action">${addButton}</a>
				</div>
				
			</form>
		</c:if>
		
		<c:if test="${user.getUserGroupId() == 3}"></c:if> <!-- student -->
		
	</div>
	
	<%@ include file="../custom/aside.jsp"%>

</div>

<%@ include file="../custom/foot.jsp"%>