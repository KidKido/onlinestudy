package com.onlinestudy.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.onlinestudy.entities.City;
import com.onlinestudy.entities.Region;

@Repository
@SuppressWarnings("unchecked")
public class LocationDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Region> getRegionsList() {
		List<Region> regionsList = sessionFactory.getCurrentSession().createCriteria(Region.class)
				.addOrder(Order.asc("name")).list();

		return regionsList;
	}

	public List<City> getCitiesList(int regionId) {
		List<City> citiesList = sessionFactory.getCurrentSession().createCriteria(City.class)
				.add(Restrictions.eq("regionId", regionId)).addOrder(Order.asc("name")).list();

		return citiesList;
	}
}
