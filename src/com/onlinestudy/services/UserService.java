package com.onlinestudy.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onlinestudy.dao.UserDao;
import com.onlinestudy.entities.User;

@Service
public class UserService {

	@Autowired
	public UserDao userDao;
	
	@Autowired
	public AuthService authService;

	@Transactional
	public Boolean saveUser(User user) {
		user.setPassword(authService.createPasswordHash(user.getPassword()));
		
		return userDao.saveUser(user);
	}
	
	@Transactional
	public User getUser(String uuid) {
		return userDao.getUser(uuid);
	}

	@Transactional
	public Integer getHomeworkAmount(String uuid) {
		return userDao.getHomeworkAmount(uuid);
	}
}
