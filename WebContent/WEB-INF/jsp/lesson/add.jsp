<%@ include file="../custom/head.jsp"%>
<%@ include file="../custom/header.jsp"%>

<div class="container">
	
	<div class="content">
		<h1 class="title">${pageTitle}</h1>
		<form class="content__form lesson-form--add" method="post" action="lessonAddCheck.action">
		
			<div class="content__form-errors"></div>
		
			<div class="content__form-row">
				<label class="label" for="subject">${subjectLabel}</label>
				<select class="input input--select" id="subject" name="subject" data-required="true">
					<option value="">${chooseOption}</option>
					<c:forEach var="each" items="${subjectsList}">
						<option value="${each.getId()}">${each.getName()}</option>
					</c:forEach>
				</select>
			</div>
			
			<div class="content__form-row">
				<label class="label" for="title">${titleLabel}</label>
				<input class="input" id="title" type="text" name="title" placeholder="${titlePlaceholder}" data-required="true" />
			</div>
			
			<div class="content__form-row">
				<label class="label" for="content">${contentLabel}</label>
				<textarea class="input input--textarea" id="content" name="content" placeholder="${contentPlaceholder}" data-required="true"></textarea>
			</div>
			
			<div class="content__form-row">
				<input class="button button--primary lesson-form--add__submit" type="submit" value="${saveButton}" />
				<a class="button button--link" href="lesson.action">${cancelButton}</a>
			</div>
			
		</form>
	</div>
	
	<%@ include file="../custom/aside.jsp"%>

</div>

<%@ include file="../custom/foot.jsp"%>