package com.onlinestudy.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlinestudy.entities.Lesson;
import com.onlinestudy.entities.Subject;
import com.onlinestudy.entities.User;
import com.onlinestudy.services.AuthService;
import com.onlinestudy.services.HomeworkService;
import com.onlinestudy.services.LessonService;
import com.onlinestudy.services.PaginationService;
import com.onlinestudy.services.SubjectService;
import com.onlinestudy.services.TeacherSubjectService;
import com.onlinestudy.services.UserService;

@Controller
public class LessonController extends BaseController {

	final private static Integer LIMIT = 5;
	final private static String TABLE = "lesson";

	@Autowired(required = false)
	private LessonService lessonService;

	@Autowired(required = false)
	private PaginationService paginationService;

	@Autowired(required = false)
	AuthService authService;
	
	@Autowired(required = false)
	private SubjectService subjectService;
	
	@Autowired(required = false)
	TeacherSubjectService teacherSubjectService;
	
	@Autowired(required = false)
	private UserService userService;
	
	@Autowired(required = false)
	HomeworkService homeworkService;

	@RequestMapping(value = "/lesson.action", method = RequestMethod.GET)
	public ModelAndView getLesson(HttpServletRequest request,
			@CookieValue(value = "uuid", required = false) String uuid,
			@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
			@RequestParam(value = "subject_id", required = false) Integer subjectId) {
		
		if (!authService.isLoggedIn(request.getCookies())) {
			return new ModelAndView("redirect:index.action");
		}

		super.initialize(uuid);

		mav.setViewName("lesson/lesson");

		paginationService.initialize();
		
		if (subjectId != null) {
			paginationService.setQueryString("subject_id", subjectId.toString());
			paginationService.setFilter("subject_id = " + subjectId);
		} else {
			subjectId = 0;
		}

		paginationService.setLink("lesson.action");
		paginationService.setCurrent(page);
		paginationService.setLimit(LIMIT);
		paginationService.setTable(TABLE);
		paginationService.run();		

		mav.addObject("pagination", paginationService);
		mav.addObject("pageTitle", "Лекції");
		mav.addObject("lessonsListEmptyLabel", "Немає лекцій.");
		mav.addObject("lessonAddLabel", "Додати лекцію");
		mav.addObject("lessonsList", lessonService.getLessonsList(paginationService.getLimit(), paginationService.getOffset(), subjectId, userService.getUser(uuid).getId()));

		return mav;
	}
	
	@RequestMapping(value = "/lessonAdd.action", method = RequestMethod.GET)
	public ModelAndView getAddSubject(@CookieValue(value = "uuid", required = false) String uuid,
			HttpServletRequest request) {

		if (!authService.isLoggedIn(request.getCookies())) {
			return new ModelAndView("redirect:index.action");
		}

		super.initialize(uuid);

		mav.setViewName("lesson/add");

		mav.addObject("pageTitle", "Додавання лекції");
		mav.addObject("subjectLabel", "Предмет");
		mav.addObject("subjectsList", subjectService.getTeacherSubjectsList(userService.getUser(uuid).getId()));
		mav.addObject("titleLabel", "Тема лекції");
		mav.addObject("titlePlaceholder", "Лекція №1");
		mav.addObject("contentLabel", "Зміст лекції");
		mav.addObject("contentPlaceholder", "Зміст...");
		mav.addObject("saveButton", "Зберегти");
		mav.addObject("cancelButton", "Скасувати");

		return mav;
	}
	
	@RequestMapping(value = "/lessonAddCheck.action", method = RequestMethod.POST)
	@ResponseBody
	public String postLessonChooseCheck(@CookieValue(value = "uuid", required = false) String uuid,
			@RequestParam(value = "subject", required = false) Integer subjectId, 
			@RequestParam(value = "title", required = false) String title, 
			@RequestParam(value = "content", required = false) String content) throws JsonProcessingException {

		List<String> errorsList = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();

		if (subjectId == null) {
			errorsList.add("Оберіть предмет.");
		}
		
		if (title == "") {
			errorsList.add("Введіть тему лекції.");
		}
		
		if (content == "") {
			errorsList.add("Введіть зміст лекції.");
		} 
		
		if (errorsList.isEmpty()) {
			Lesson lesson = new Lesson();

			User teacher = new User();
			teacher.setId(userService.getUser(uuid).getId());
			lesson.setTeacher(teacher);
			
			lesson.setCreationDate(new Date());

			Subject subject = new Subject();
			subject.setId(subjectId);
			lesson.setSubject(subject);
			
			lesson.setTitle(title);
			
			lesson.setContent(content);

			if (!lessonService.save(lesson)) {
				errorsList.add("Помилка додавання лекції.");
			}
		}

		return mapper.writeValueAsString(errorsList);
	}
	
	@RequestMapping(value = "/lessonView.action", method = RequestMethod.GET)
	public ModelAndView getLessonView(@CookieValue(value = "uuid", required = false) String uuid,
			@RequestParam(value = "lesson_id", required = false) Integer lessonId,
			HttpServletRequest request) {

		if (!authService.isLoggedIn(request.getCookies())) {
			return new ModelAndView("redirect:index.action");
		}

		if (lessonId == null) {
			return new ModelAndView("redirect:lesson.action");
		}
		
		super.initialize(uuid);

		mav.setViewName("lesson/view");

		Lesson lesson = lessonService.getLesson(lessonId);
		
		mav.addObject("pageTitle", lesson.getTitle());
		mav.addObject("homeworkAddLabel", "Додати домашнє завдання");
		mav.addObject("lesson", lesson);
		mav.addObject("homeworkTitle", "Домашні завдання");
		mav.addObject("homeworksList", homeworkService.getHomeworksList(lesson.getId()));
		mav.addObject("homeworkAddAnswerLabel", "Додати відповідь");
		mav.addObject("titleLabel", "Тема лекції");
		mav.addObject("titlePlaceholder", "Лекція №1");
		mav.addObject("contentLabel", "Зміст лекції");
		mav.addObject("contentPlaceholder", "Зміст лекції...");
		mav.addObject("saveButton", "Зберегти");
		mav.addObject("cancelButton", "Скасувати");

		return mav;
	}

}
