package com.onlinestudy.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class PaginationDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Transactional
	public int getTotalAmount(String table, String filter) {

		String sql = "SELECT COUNT(*) AS total FROM " + table;

		if (filter != null) {
			sql += " WHERE " + filter;
		}

		Number number = (Number) sessionFactory.getCurrentSession().createSQLQuery(sql).uniqueResult();

		return number.intValue();
	}

}
