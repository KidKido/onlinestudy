package com.onlinestudy.dao;

import java.io.Serializable;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.onlinestudy.entities.TeacherSubject;

@Repository
public class TeacherSubjectDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	public Boolean hasSubject(Integer subjectId, Integer teacherId) {
		Object hasSubject = sessionFactory.getCurrentSession().createCriteria(TeacherSubject.class)
				.add(Restrictions.eq("subjectId", subjectId)).add(Restrictions.eq("teacherId", teacherId)).uniqueResult();

		return hasSubject != null;
	}

	public Boolean save(TeacherSubject teacherSubject) {
		Serializable result = sessionFactory.getCurrentSession().save(teacherSubject);
		sessionFactory.getCurrentSession().flush();
		
		return (result != null);
	}

}
