package com.onlinestudy.dao;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.onlinestudy.entities.User;

@Repository
public class AuthDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Boolean isLoggedIn(String uuid) {
		Object loggedIn = sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.eq("uuid", uuid)).uniqueResult();

		return (loggedIn != null);
	}

	public Boolean checkLogin(String email, String password) {
		Object uniqueResult = sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.eq("email", email)).add(Restrictions.eq("password", password)).uniqueResult();

		return (uniqueResult != null);
	}

	public void saveUuid(String email, String uuid) {
		Session session = sessionFactory.getCurrentSession();

		User user = (User) session.createCriteria(User.class).add(Restrictions.eq("email", email)).uniqueResult();

		user.setUuid(uuid);

		session.update(user);
		session.flush();
	}

	public Boolean logout(String uuid) {
		Session session = sessionFactory.getCurrentSession();

		Query query = session.createQuery("UPDATE User SET uuid = '' WHERE uuid = :uuid").setMaxResults(1);
		query.setParameter("uuid", uuid);

		return (query.executeUpdate() != 0);
	}

	public Boolean isAvailableEmail(String email) {
		Object isAvailableEmail = sessionFactory.getCurrentSession().createCriteria(User.class)
				.add(Restrictions.eq("email", email)).uniqueResult();

		return (isAvailableEmail == null);
	}
}
