package com.onlinestudy.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlinestudy.entities.City;
import com.onlinestudy.entities.Region;
import com.onlinestudy.entities.User;
import com.onlinestudy.services.AuthService;
import com.onlinestudy.services.LocationService;
import com.onlinestudy.services.UserService;
import com.steadystate.css.parser.ParseException;

@Controller
public class RegistrationController extends BaseController {

	@Autowired(required = false)
	private LocationService locationService;

	@Autowired(required = false)
	private UserService userService;

	@Autowired(required = false)
	private AuthService authService;

	@RequestMapping(value = "/registration.action", method = RequestMethod.GET)
	public ModelAndView getRegistration(
			@CookieValue(value = "uuid", required = false) String uuid,
			HttpServletRequest request) {
		
		if (authService.isLoggedIn(request.getCookies())) {
			return new ModelAndView("redirect:index.action");
		}
		
		super.initialize(uuid);
		
		mav.setViewName("auth/registration");

		mav.addObject("pageTitle", "Реєстрація");

		mav.addObject("groupLabel", "Тип");
		mav.addObject("groupTeacherOption", "Викладач");
		mav.addObject("groupStudentOption", "Студент");

		mav.addObject("emailLabel", "Електронна пошта");
		mav.addObject("emailPlaceholder", "example@ukr.net");

		mav.addObject("passwordLabel", "Пароль");
		mav.addObject("passwordRepeatLabel", "Повторіть пароль");

		mav.addObject("nameLabel", "ПІБ");
		mav.addObject("namePlaceholder", "Іванов Іван Іванович");

		mav.addObject("genderLabel", "Стать");
		mav.addObject("genderFemaleOption", "Жіноча");
		mav.addObject("genderMaleOption", "Чоловіча");

		mav.addObject("birthDateLabel", "Дата народження");
		mav.addObject("birthDatePlaceholder", "31.12.1970");

		mav.addObject("regionLabel", "Регіон");
		mav.addObject("regionsList", locationService.getRegionsList());

		mav.addObject("cityLabel", "Місто");

		mav.addObject("registerButton", "Зареєструватися");
		mav.addObject("cancelButton", "Скасувати");

		mav.addObject("informationTitle", "Увага!");
		mav.addObject("informationText", "Всі поля обов'язкові для заповнення.");

		return mav;
	}

	@RequestMapping(value = "/registrationCheck.action", method = RequestMethod.POST)
	@ResponseBody
	public String postCheckRegistration(
			HttpServletResponse response,
			@RequestParam(value = "group", required = false) Integer group,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "password", required = false) String password,
			@RequestParam(value = "passwordRepeat", required = false) String passwordRepeat,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "gender", required = false) Integer gender,
			@RequestParam(value = "birthDate", required = false) String birthDate,
			@RequestParam(value = "region", required = false) Integer regionId,
			@RequestParam(value = "city", required = false) Integer cityId)
					throws ParseException, JsonProcessingException, java.text.ParseException {

		List<String> errorsList = new ArrayList<>();
		ObjectMapper mapper = new ObjectMapper();

		if (group == null) {
			errorsList.add("Оберіть тип групи.");
		}

		if (email == "") {
			errorsList.add("Введіть електронну пошту.");
		} else {
			Pattern pattern = Pattern.compile("^[0-9a-z.-_]+@[0-9a-z.-]+\\.[0-9a-z-]{2,}$", Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(email);
			
			if (!matcher.matches()) {
				errorsList.add("Некоректне введення електронної пошти.");
			} else if (!authService.isAvailableEmail(email)) {
				errorsList.add("Така електронна пошта вже зареєстрована.");
			}
		}

		if (password == "") {
			errorsList.add("Введіть пароль.");
		} else {
			Pattern pattern = Pattern.compile("^[0-9a-z.-_]{6,16}$", Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(password);
			
			if (!matcher.matches()) {
				errorsList.add("Пароль може містити тільки латинські літери, цифри, символи(.-_) та мати довжину від 6 до 16 символів.");
			}
		}

		if (passwordRepeat == "") {
			errorsList.add("Повторіть введений пароль.");
		} else if (!password.equals(passwordRepeat)) {
			errorsList.add("Паролі повинні співпадати.");
		}

		if (name == "") {
			errorsList.add("Введіть ПІБ.");
		} else {
			Pattern pattern = Pattern.compile("^[\\s\\p{Alpha}-']{3,}$", Pattern.UNICODE_CHARACTER_CLASS);
			Matcher matcher = pattern.matcher(name);
			
			if (!matcher.matches()) {
				errorsList.add("Некоректне введення ПІБ.");
			}
		}

		if (gender == null) {
			errorsList.add("Оберіть стать.");
		}

		if (birthDate == "") {
			errorsList.add("Оберіть дату народження.");
		} else {
			Pattern pattern = Pattern.compile("^(0[1-9]|1[0-9]|2[0-9]|3[0-1])\\.(0[1-9]|1[1-2])\\.(19|20)[0-9]{2}$");
			Matcher matcher = pattern.matcher(birthDate);
			
			if (!matcher.matches()) {
				errorsList.add("Дата народження повинна бути в форматі ДД.ММ.РРРР.");
			}
		}

		if (regionId == null) {
			errorsList.add("Оберіть регіон.");
		} 

		if (cityId == null) {
			errorsList.add("Оберіть місто.");
		}
		
		if (errorsList.isEmpty()) {
			 User user = new User();
			
			 user.setUserGroupId(group);
			 user.setEmail(email);
			 user.setPassword(password);
			 user.setName(name);
			 user.setGender(gender);
			 user.setBirthDate(new SimpleDateFormat("dd.MM.yyyy").parse(birthDate));
			
			 Region region = new Region();
			 region.setId(regionId);
			 user.setRegion(region);
			
			 City city = new City();
			 city.setId(cityId);
			 user.setCity(city);
			
			 if (userService.saveUser(user)) {
				 response.addCookie(authService.login(email, password));
			 } else {
				 errorsList.add("Помилка реєстрації користувача.");
			 }
		}

		return mapper.writeValueAsString(errorsList);
	}
}
