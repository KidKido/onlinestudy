package com.onlinestudy.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onlinestudy.dao.LocationDao;
import com.onlinestudy.entities.City;
import com.onlinestudy.entities.Region;

@Service
public class LocationService {

	@Autowired
	public LocationDao locationDao;

	@Transactional
	public List<Region> getRegionsList() {
		return locationDao.getRegionsList();
	}

	@Transactional
	public List<City> getCitiesList(int regionId) {
		return locationDao.getCitiesList(regionId);
	}

}
