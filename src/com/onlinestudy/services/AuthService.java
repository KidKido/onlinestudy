package com.onlinestudy.services;

import java.util.UUID;

import javax.servlet.http.Cookie;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.onlinestudy.dao.AuthDao;

@Service
public class AuthService {
	final private static int COOKIE_EXPIRE = 86400;

	@Autowired
	AuthDao authDao;
	
	@Transactional
	public boolean isLoggedIn(Cookie[] cookies) {
		for (Cookie each : cookies) {
			if (each.getName().equals("uuid")) {
				String uuid = each.getValue();

				return authDao.isLoggedIn(uuid);
			}
		}

		return false;
	}

	@Transactional
	public Boolean checkLogin(String email, String password) {
		return authDao.checkLogin(email, createPasswordHash(password));
	}

	@Transactional
	public Cookie login(String email, String password) {
		String uuid = UUID.randomUUID().toString();
		
		authDao.saveUuid(email, uuid);
		
		Cookie cookie = new Cookie("uuid", uuid);
		cookie.setMaxAge(COOKIE_EXPIRE);
		
		return cookie;
	}
	
	@Transactional
	public Cookie logout(Cookie[] cookies) {
		for (Cookie each : cookies) {
			if (each.getName().equals("uuid")) {
				String uuid = each.getValue();

				if (authDao.logout(uuid)) {
					each.setValue("");
					each.setMaxAge(0);

					return each;
				}

				break;
			}
		}

		return null;
	}

	@Transactional
	public boolean isAvailableEmail(String email) {
		return authDao.isAvailableEmail(email);
	}

	@Transactional
	public String createPasswordHash(String password) {
		return DigestUtils.sha1Hex(password);
	}
}
